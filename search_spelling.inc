<?php

/**
 * Settings form.
 */
function search_spelling_settings_form($form, &$form_state) {
  $form['search_spelling_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => variable_get('search_spelling_key'),
    '#description' => t('Your Google API key. <a href="@url">How to get it</a>.', array('@url' => 'https://developers.google.com/custom-search/v1/getting_started#auth')),
  );
  
  $form['search_spelling_cx'] = array(
    '#type' => 'textfield',
    '#title' => t('Search engine unique ID'),
    '#default_value' => variable_get('search_spelling_cx'),
    '#description' => t('Your search engine unique ID. <a href="@url">How to get it</a>.', array('@url' => 'https://developers.google.com/custom-search/v1/getting_started#set_up_cse')),
  );
  
  return system_settings_form($form);
}

/**
 * No results post render callback.
 */
function search_spelling_no_results_post_render($content, $element) {
  $check_spelling_result = search_spelling_check(arg(2));
  if ($check_spelling_result) {
    $link = l($check_spelling_result, 'search/' . arg(1) . '/' . $check_spelling_result);
    $content = str_replace('<ul>', '<ul><li>' . t('Did you mean: !link', array('!link' => $link)) . '</li>', $content);
  }
  return $content;
}

/**
 * Check spelling.
 */
function search_spelling_check($phrase) {
  $key = variable_get('search_spelling_key');
  $cx = variable_get('search_spelling_cx');
  if (!$key || !$cx) {
    return NULL;
  }
  
  $request_result = drupal_http_request('https://www.googleapis.com/customsearch/v1?key=' . $key . '&cx=' . $cx . '&q=' . $phrase);
  if ($request_result->code == 200) {
    $request_result = json_decode($request_result->data);
    if (!empty($request_result->spelling->correctedQuery)) {
      return $request_result->spelling->correctedQuery;
    }
  }
}
